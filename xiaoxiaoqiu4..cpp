#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h>

#define High 15		//游戏画面大小
#define Width 20 

//全局变量
int ball_x,ball_y;	//小球坐标
int ball_vx,ball_vy;	//小球的速度
int position_x,position_y;	//挡板的中心坐标
int ridus;	//挡板的半径大小
int left,right;	//挡板的左右位置
int canvas[High][Width]={0};	//二维数组储存游戏画布中对应的元素
//0为空格，1为小球，2为挡板* ,3为砖块# 

//消除屏闪，但不能消除光标闪烁问题 
void gotoxy(int x,int y)	//将光标移动到(x,y)位置 
{
	HANDLE handle=GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X=x;
	pos.Y=y;
	SetConsoleCursorPosition(handle,pos); 
}

//消除光标闪烁问题 
void HideCursor()
{
	CONSOLE_CURSOR_INFO cursor_info={1,0};
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE),&cursor_info);
 } 
 
void startup()
{
	ridus=5; 
	position_x=High;
	position_y=Width/2;
	left=position_y-ridus;
	right=position_y+ridus;
	
	ball_x=position_x-1;
	ball_y=position_y;
	ball_vx=-1;
	ball_vy=1;
	canvas[ball_x][ball_y]=1;
	
	int k,i;
	for(k=left;k<=right;k++)	//挡板 
		canvas[position_x][k]=2;
		
	for(k=0;k<Width;k++)	//加几排砖块 
		for(i=0;i<High/4;i++)
			canvas[i][k]=3;
} 

void show()	//显示画面 
{
	gotoxy(0,0);	//将光标移动到原点位置，以下重画清屏 
	int i,j;
	for(i=0;i<=High;i++)
	{
		for(j=0;j<Width;j++)
		{
			if(canvas[i][j]==0)
				printf(" ");	//输出空格 
			else if(canvas[i][j]==1)
				printf("0");	//输出小球0 
			else if(canvas[i][j]==2)
				printf("*");	//输出挡板* 
			else if(canvas[i][j]==3)
				printf("#");	//输出砖块# 
		}
		printf("|\n");	//输出右边界 
	}
	for(j=0;j<Width;j++)
		printf("-");	//输出下边界 
	printf("\n"); 
}

void updateWithoutInput()	//与用户输入无关的更新 
{
	if(ball_x==High-2)
	{
		if((ball_y>=left)&&(ball_y<=right))	//球被挡板挡住 
		{
			printf("\a");	//响铃 
		}
		else
		{
			printf("游戏失败\n");
			system("pause");	//而pause是DOS下的一个命令,就是暂停程序的执行，等待任意健继续执行。
			exit(0);
		 } 
	}
	
	static int speed=0;
	if(speed<7)	//减缓小球运动速度，只有当程序循环7次后，小球才会移动一次，和Speed()的效果一样 
		speed++;
	if(speed==7)
	{
		speed=0;
		
		canvas[ball_x][ball_y]=0;
		//更新小球坐标 
		ball_x=ball_x+ball_vx;
		ball_y=ball_y+ball_vy;
		
		//碰到边界后反弹 
		if((ball_x==0)||(ball_x==High-1))	//小球遇到上下边界反弹 
			ball_vx=-ball_vx;
		if((ball_y==0)||(ball_y==Width-1))	//小球遇到左右边界反弹 
			ball_vy=-ball_vy;
		canvas[ball_x][ball_y]=1;
			
		//碰到砖块后反弹
		if(canvas[ball_x-1][ball_y]==3)
		{
			ball_vx=-ball_vx;
			canvas[ball_x-1][ball_y]=0;
			printf("\a");
		}
	 } 
}

void updateWithInput()	//与用户输入有关的更新 
{
	char input;
	if(kbhit())
	{
		input=getch();
		if(input=='a'&&left>0)
		{
			canvas[position_x][right]=0;
			position_y--;	//位置右移 
			left=position_y-ridus;
			right=position_y+ridus;
			canvas[position_x][left]=2;
		}
		if(input=='d'&&right<Width-1)
		{
			canvas[position_x][left]=0;
			position_y++;	//位置左移 
			left=position_y-ridus;
			right=position_y+ridus;
			canvas[position_x][right]=2;
		}
	}
}

int main()
{
	HideCursor(); 
	startup();
	while(1)
	{
		show();
		updateWithoutInput();
		updateWithInput();
	}
	return 0;
}					
